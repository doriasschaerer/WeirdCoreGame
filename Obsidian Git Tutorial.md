### How to use Git with Obsidian.md

After you're done with what you're currently working on. Execute this command:

- Obsidian Git: Create backup with specific message

Or press **CTRL + SHIFT + B**

Then, write what you worked on.

If you want to get the latest version of the vault, do:

- Obsidian Git: Pull